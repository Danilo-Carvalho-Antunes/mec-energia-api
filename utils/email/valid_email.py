import re

def validar_email(email):
    aux =  r'^[\w\.-]+@[\w\.-]+\.\w+$'

    if re.match(aux, email):
        return True
    else:
        return False

def test_validar_email():
    assert validar_email('teste@exemplo.com') == True
    assert validar_email('teste@exemplo') == False